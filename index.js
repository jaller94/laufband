'use strict';
//requiring path and fs modules
const path = require('path');
const fs = require('fs');

const analyzeGpxFile = require('./gpx-helpers.js');

//joining path of directory
const directoryPath = path.join(__dirname, 'tracks');
//passsing directoryPath and callback function
fs.readdir(directoryPath, function (err, files) {
  // handling error
  if (err) {
    return console.log('Unable to scan directory: ' + err);
  }
  const stats = {
    totalDistance: 0,
    sessions: [],
  };
  // listing all files using forEach
  files.forEach((file) => {
    // Do whatever you want to do with the file
    analyzeGpxFile(path.join(directoryPath, file), stats);
  });
  console.dir(stats);
});
