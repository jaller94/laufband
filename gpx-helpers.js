'use strict';
const fs = require('fs');
const convert = require('xml-js');

function getElementByName(root, name) {
  if (!root) throw new Error('Cannot inspect undefined!');
  return root.elements.find((ast) => ast.type === 'element' && ast.name === name);
}

function getElementsByName(root, name) {
  if (!root) throw new Error('Cannot inspect undefined!');
  return root.elements.filter((ast) => ast.type === 'element' && ast.name === name);
}

function requireElementByName(root, name) {
  const found = getElementByName(root, name);
  if (!found) throw new Error(`Could not find ${name}.`);
  return found;
}

function getText(root) {
  const textElement = root.elements.find((ast) => ast.type === 'text');
  if (textElement) return textElement.text;
}

function setText(root, text) {
  const textElement = root.elements.find((ast) => ast.type === 'text');
  if (!textElement) throw new Error('Element has no text child.');
  textElement.text = text;
}

function analyzeGpxFile(path, stats) {
  const xml = fs.readFileSync(path, 'utf8');
  const root = convert.xml2js(xml);
  const gpx = requireElementByName(root, 'gpx');
  const track = requireElementByName(gpx, 'trk');
  const segments = getElementsByName(track, 'trkseg');

  let totalDistance = 0;
  let totalDuration = 0;
  segments.forEach((segment) => {
    let lastPoint;
    const points = segment.elements.filter((element) => {
      return element.type === 'element' && element.name === 'trkpt';
    });
    points.forEach((point) => {
      if (!point.attributes) {
        console.dir(point);
        console.warn(`${path} has a point without attributes!`);
        return;
      }
      if (!point.attributes.lat) {
        console.warn(`${path} has a point without lat value!`);
        return;
      }
      let currentPoint = {
        lat: point.attributes.lat,
        lon: point.attributes.lon,
        time: new Date(getText(requireElementByName(point, 'time'))),
      };
      if (lastPoint) {
        let timeDelta = (currentPoint.time.getTime() - lastPoint.time.getTime()) / 1000;
        let distanceDelta = distance(lastPoint.lat, lastPoint.lon, currentPoint.lat, currentPoint.lon, 'K') * 1000;
        if (distanceDelta > 0.01) {
          // console.log(currentPoint.time, timeDelta, distanceDelta.toFixed(2), distanceDelta / timeDelta);
        }
        totalDistance += distanceDelta;
        totalDuration += timeDelta;
      }
      lastPoint = currentPoint;
      //console.log(point);
    });
  });

  stats.totalDistance += totalDistance;
  stats.sessions.push({
    path,
    totalDistance,
    totalDuration,
  });
}

module.exports = analyzeGpxFile;

//console.log(segment);


//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
//:::                                                                         :::
//:::  This routine calculates the distance between two points (given the     :::
//:::  latitude/longitude of those points). It is being used to calculate     :::
//:::  the distance between two locations using GeoDataSource (TM) prodducts  :::
//:::                                                                         :::
//:::  Definitions:                                                           :::
//:::    South latitudes are negative, east longitudes are positive           :::
//:::                                                                         :::
//:::  Passed to function:                                                    :::
//:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
//:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
//:::    unit = the unit you desire for results                               :::
//:::           where: 'M' is statute miles (default)                         :::
//:::                  'K' is kilometers                                      :::
//:::                  'N' is nautical miles                                  :::
//:::                                                                         :::
//:::  Worldwide cities and other features databases with latitude longitude  :::
//:::  are available at https://www.geodatasource.com                         :::
//:::                                                                         :::
//:::  For enquiries, please contact sales@geodatasource.com                  :::
//:::                                                                         :::
//:::  Official Web site: https://www.geodatasource.com                       :::
//:::                                                                         :::
//:::               GeoDataSource.com (C) All Rights Reserved 2018            :::
//:::                                                                         :::
//:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
function distance(lat1, lon1, lat2, lon2, unit) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		if (unit=="K") { dist = dist * 1.609344 }
		if (unit=="N") { dist = dist * 0.8684 }
		return dist;
	}
}

//const craftingRecipes = requireElementByName(player, 'craftingRecipes');
//setText(playerName, 'Jaller');

//craftingRecipes.elements.forEach(element => {
//    console.log(requireElementByName(element, 'key'));
//});
